package com.greatpd.restful.springboottest.controller;

import com.greatpd.restful.springboottest.domain.Parame;
import com.greatpd.restful.springboottest.response.JSONResponse;
import com.sample.springboot.domain.Hello;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.tags.Param;

@RestController
// http://localhost:8080/hello/ 안에 걸림
@RequestMapping(value = "hello")
public class HelloController {
    // http://localhost:8080/hello
    @GetMapping("")
    public Hello get(){
        return new Hello(0,"Hello Spring!");
    }

    // http://localhost:8080/hello/args?mgs=값&mgs2=값
    // get 방식
    @GetMapping("args")
    public Hello getArgs(
            @RequestParam(value = "msg") String msg,
            @RequestParam(value = "mgs2") String mgs2
    ){
        Hello hello = new Hello(0,msg);
        hello.setMsg(hello.getMsg()+" , " + mgs2);
        return hello;
    }

    @GetMapping("args2")
    // 파라미터 값을 받지만, 필수값 아닌경우 디폴트 설정가능
    public Hello getArgs2(
            @RequestParam(value = "msg") String msg,
            @RequestParam(value = "mgs2" , required = false, defaultValue = "mgs2Default") String mgs2
    ){
        Hello hello = new Hello(0,msg);
        hello.setMsg(hello.getMsg()+" , " + mgs2);
        return hello;
    }

    @GetMapping("jsonResponse")
    // return 타입 클래스를 공통으로 만들어서 세팅하고 전달함
    public JSONResponse<Hello> getJsonResponse(
            @RequestParam(value="msg")String msg
    ){
        Hello hello = new Hello(0,msg);
        JSONResponse<Hello> response = new JSONResponse<Hello>();
        response.setCode(1);
        response.setMsg("응답코드가 1");
        response.setData(hello);
        return response;
    }

    // post 전송 requestBody 를 받음
    @PostMapping("login")
    public Hello post(@RequestBody Parame parame){
        // 선언한 변수명이 파라미터 값이 된다.
        String msg = "Data1:"+ parame.getData1() + ", Data2 : " + parame.getData2();
        Hello hello = new Hello(0,msg);
        return hello;
    }

    // GET 조회, POST 수정, PUT 추가, DELETE 삭제
    // 이렇게 쓸수 있다.
    @PutMapping("add")
    public Hello put(@RequestParam(value="param1")String param1){
        Hello hello = new Hello(0,"put Request:" + param1);
        return hello;
    }


}
