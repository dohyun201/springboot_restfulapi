package com.greatpd.restful.springboottest.response;

// 응답 공통 클래스
public class JSONResponse<T> {
    int code;
    String msg;
    T data;

    public JSONResponse(){
    }

    public String getMsg() {
        return msg;
    }

    public int getCode() {
        return code;
    }

    public T getData() {
        return data;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setData(T data) {
        this.data = data;
    }
}
